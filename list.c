#include <stdio.h>
#include <stdlib.h>

typedef struct node NODE;
typedef struct head HEAD;

struct head{
	NODE* head;
};

struct node{
	int data;
	NODE* next;
};/*

int List_Init(NODE *head){
	return head->head = NULL;
}
*/
int Is_Empty(HEAD *head){
	if (head->head == NULL)
	{
		return 1;
	}
	return 0;
}


NODE* Find(HEAD *head, int data){

	NODE *pont = head->head;
	
	while(pont != NULL && pont->data != data){
		pont = pont->next;
	}
	return pont;

}

int Insert_Final(HEAD *head, int data){
	NODE *pont = malloc(sizeof(NODE));
	
	if (pont != NULL)
	{
		pont->data = data;
	}

	if (Is_Empty(head))
	{
		head->head = pont;
		pont->next = NULL;
	}

	NODE *temp = head->head;
	while(temp->next != NULL){
		temp = temp->next;
	}
	
	temp->next = pont;
	pont->next = NULL;

}

void List_Free(HEAD* head){
	
	NODE *pont = head->head;
	
	while(pont != NULL){
		NODE *temp = pont->next;
		free(pont);
		pont = temp;


	}
	head->head = NULL;

}

void Insert_Head(HEAD *head, int data){
	
	NODE *pont = (NODE*) malloc(sizeof(NODE));
	NODE *temp = head->head;
	
	if (pont != NULL)
	{
		pont->data = data;
		pont->next = temp; 
		head->head = pont;
	}
	
}

/*
int Delete(HEAD *head, int data){
	
	NODE *pont = head->head;
	
	if (pont == NULL){
		return 0;
	}else if (pont->data == data){
		NODE *no = pont->next;
		head->head = no;
		free(no);
		return 1;
	}

	while(pont->next != NULL && pont->next->data != data){
		pont = pont->next;
	}

	if (pont->data == data){
		NODE *no = pont->next;
		head->head = no;
		free(no);
		return 1;
	
	NODE *no = pont->next;
	pont-next = no->next;
	free(no);

	return 0;

	
}

*/

void Print(HEAD *head){
	
	NODE *pont = head->head;

	while(pont != NULL){
		printf("Numero: %d\n", pont->data);
		pont = pont->next;
	
	}
}

void menu(int op){
	HEAD* head;
	
	while(op){
		printf("1 - Criar Lista\n");
		printf("2 - Imprimir Lista\n");
		printf("3 - Inserir Elementos No Inicio\n");
		printf("4 - Inserir Elementos No Final\n");
		printf("5 - Buscar Algum Elemento\n");
		printf("6 - Liberar Lista\n");
		scanf("%d", &op);
		switch (op){
			int data;
			case 1:

				head = (HEAD*) malloc(sizeof(HEAD));
				if (head != NULL)
				{
					printf("Lista Alocada com sucesso\n");
					
				}else{
					printf("Lista Não Alocada\n");
				}
				break;
			case 2:
				Print(head);
				printf("\n\n\n");
				break;
			case 3:
				printf("Digite um numero\n");
				scanf("%d", &data);
				Insert_Head(head, data);
				printf("\n\n\n");
				break;
			case 4:
				printf("Digite um numero\n");
				scanf("%d", &data);
				Insert_Final(head, data);
				printf("\n\n\n");
				break;
			


			}
		
	}
	
}

int main(int argc, char const *argv[])
{
	
	
	int op = 1;
	printf("Digite 1 pra ir no MENU\n");
	printf("Digite qualquer outro numero para sair\n");
	scanf("%d", &op);
	switch(op){
		case 1:
			menu(op);
			break;
		default:
			printf("Opção Invalida, programa encerrado\n");
			break;
	}
	return 0;
}