#include <stdio.h>
#include <stdlib.h>

typedef struct node NODE;
typedef struct top TOPO;

struct top
{
	NODE *node;
};

struct node
{
	int data;
	NODE *next;
};

void Stack_Init(TOPO *top){
	top->node = NULL;
}

int Is_Empty(TOPO *top){
	
	if (top->node == NULL)
	{
		return 1;
	}
	return 0;

}

void Push(TOPO *top){
	
	
	printf("Digite um numero: \n");
	int data;
	scanf("%d", &data);

	NODE *pont = malloc(sizeof(NODE));
	
	if (pont == NULL)
	{
		printf("Operação Mal-Sucedida\n");
	}else{
		pont->data = data;
		pont->next = top->node;
		top->node = pont;
		printf("Inserção Feita com Sucesso\n");
	}

}

void Pop(TOPO *top){
	
	if (Is_Empty(top))
	{
		printf("Pilha Vazia\n");
	}else{

		NODE *pont = top->node;
		top->node = pont->next;
		free(pont);
		pont = NULL;
	}
	
}

void Print(TOPO *top){

	if (Is_Empty(top))
	{
		printf("Pilha Vazia\n");
	}else{
		NODE *pont = top->node;
		while(pont != NULL){
			printf("%d ", pont->data);
			pont = pont->next;
		}
	}

}

NODE* FInd(TOPO *top){
	
}

void Stack_Free(TOPO *top){

	if (Is_Empty(top))
	{
		printf("Pilha\n");
	}else{
		NODE *pont = top->node;
		while(pont != NULL){
			NODE *temp = pont->next;
			free(pont);
			pont = temp;

		}
		Stack_Init(top);
	}

}

void menu(int op, TOPO *top){
	
	while(op){
		printf("\n1 - Imprimir Lista\n");
		printf("2 - Inserir Elemento\n");
		printf("3 - Buscar Algum Elemento\n");
		printf("4 - Retirar Um Elemento da Pilha\n");
		printf("5 - Liberar Lista\n");
		scanf("%d", &op);
		
		switch (op){
			int data;
			case 1:
				Print(top);
				break;
			case 2:
				Push(top);
				break;
			case 3:
				Find(top);
				break;
			case 4:
				Pop(top);
				break;
			case 5:
				Stack_Free(top);
				break;
			}
	}
	
}

int main(int argc, char const *argv[])
{
	
	
	int op = 1;
	printf("Digite 1 pra ir no MENU\n");
	printf("Digite qualquer outro numero para sair\n");
	scanf("%d", &op);
	
	TOPO* top = malloc(sizeof(TOPO));
	if (top != NULL)
	{	
		Stack_Init(top);
		switch(op){
			case 1:
				menu(op, top);
				break;
			default:
				printf("Opção Invalida, programa encerrado\n");
				break;
		}
		
	}else printf("Programa Encerrado!\n");
}






















